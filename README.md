Ghost HTML5 Backend
==================

HTML5 Backend-Rip of the [Ghost](https://github.com/TryGhost/Ghost) CMS
  
##Why?
I´ve ripped this "theme" because i´m working on a php-fork of Ghost.  
It´s not official but i really want to make it feel like Ghost itself. 

##Screenshots:
#####Login Screen:
![image](https://photos-3.dropbox.com/t/0/AACl89eXDThNaD1EG0D23QsDDaJZQKhCeRrOlMXYMFBArg/12/19731175/png/1024x768/3/1397754000/0/2/Screenshot%202014-04-17%2017.55.02.png/LJRjASzRHBJiRFeirYIO4shX5249R0NzWNfSqeo-kfg)

#####Dashboard:
![image](https://photos-5.dropbox.com/t/0/AAAGeZHaM322FEH5sExrOulje--rmNoz1nKUsr5CdLQXeQ/12/19731175/png/1024x768/3/1397754000/0/2/Screenshot%202014-04-17%2017.55.10.png/HFQdfcYwgbc4W5ZUa9rmCaxJnfiyP_DuJME4ZaEhais)

#####Adding new article:
![image](https://photos-3.dropbox.com/t/0/AADEQQjjakrxEYAbg3enHAFjfnZxs5yDcgPN2gLbz0JU2Q/12/19731175/png/1024x768/3/1397754000/0/2/Screenshot%202014-04-17%2017.55.06.png/80NnRznwW4QeDWIwWC0_kz4jTK168qf0JRfGwaY73L0)

#####Settings:
![image](https://photos-5.dropbox.com/t/0/AAD8QtZf0dVpqZCKT8pb8StL921wWYTB5jEywtvIkf7rKg/12/19731175/png/1024x768/3/1397754000/0/2/Screenshot%202014-04-17%2017.55.14.png/xgBLSM-FJDPSlc7JwShfwlsypgfH0IpnX1aWJUZqja0)

##NOTE: These files are only ripped from the Ghost-CMS. 